"use strict";

var utils = require("../utils/writer.js");
var DefaultRule = require("../models/DefaultRule.model.js");
var CustomRule = require("../models/CustomRule.model.js");
let config = require("../config/config");
let middleware = require("../utils/middleware");

const moment = require("moment");
moment().format();

// Create a default rule and save to database
module.exports.addDefaultRule = function addDefaultRule(req, res, next) {
  var body = req.swagger.params["body"].value;

  // Create variable to store slots
  const slots = [];

  if (!body.closed && !body.lunch_break) {
    // If day is not closed and no lunch break

    // Create moment objects of first checkin and last checkin
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );

    // Create slots between the first and last checkin
    while (first <= last) {
      // Convert checkin time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the create slot in the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add the duration into the first checkin time to move to next slot and repeat the process
      first.add(body.slot_default_duration, "minutes");
    }
  } else if (!body.closed && body.lunch_break) {
    // If day is not closed and there is a lunch break

    // Create moment objects of first and last checkin and the lunch time
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );
    const lunch = moment(
      `${body.lunch_break_from.hours}:${body.lunch_break_from.minutes}`,
      "HH:mm"
    );

    // Create slots between first checkin and lunch
    while (first <= lunch) {
      // Convert checkin_time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the duration of a slot into checkin time
      first.add(body.slot_default_duration, "minutes");

      // If checkout time exceeds the lunch time, then it is not a valid slot. break the loop
      if (first > lunch) break;
      else first.subtract(body.slot_default_duration, "minutes");

      // Add the slot into the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into first checkin to move to next slot
      first.add(body.slot_default_duration, "minutes");
    }

    // Add the lunch break duration into lunch time to create slots after lunch
    lunch.add(body.lunch_break_duration, "minutes");

    // Create slots between lunch end and last checkin
    while (lunch <= last) {
      // Convert checkin_time to object
      const checkin_time = new moment(lunch).format("HH:mm").split(":");

      // Add the slot to the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into lunch end to move to next slot
      lunch.add(body.slot_default_duration, "minutes");
    }
  }
  if (slots.length == 0) {
    utils.writeJson(res, { message: "No slots created." }, 500);
  }
  // Store created slots in the rule object
  body.slots = slots;

  // Create the rule
  const rule = new DefaultRule();
/*   DefaultRule
    .find({day: {
    month: body.day.month,
    weekday: `${body.day.weekday}`
  }}, function(err, user) {
    if (err) throw err;
  
    // object of the user
    console.log(user);
  }); */
  // Save it into database 
  DefaultRule
    .findOneAndUpdate({day: {
      month: body.day.month,
      weekday: `${body.day.weekday}`,
      location_id: body.location_id
    }},{$set:body}, { upsert: true ,useFindAndModify: false,new: true})
    .then(rule => {
      // Return the created rule
      utils.writeJson(res, rule);
    })
    .catch(/* istanbul ignore next */ err => {
      // Handle any unexpected error
      utils.writeJson(res, { message: err.message }, 500);
    });
};

// Delete a default rule given the id
module.exports.deleteDefaultRule = function deleteDefaultRule(req, res, next) {
  var ruleId = req.swagger.params["ruleId"].value;

  // Find the rule in mongo using id, then delete
  DefaultRule.findByIdAndDelete(ruleId)
    .then(rule => {
      // If no rule found with given id, return 404
      if (!rule) {
        utils.writeJson(res, `Rule not found with id ${ruleId}.`, 404);
      }

      // Return the default rule
      utils.writeJson(res, rule);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error deleting rule with id ${ruleId}.`,
        500
      );
    });
};

// Get all default rules from mongo
module.exports.getAllDefaultRules = function getAllDefaultRules(
  req,
  res,
  next
) {
  // Get all rules
  DefaultRule.find()
    .then(rules => {
      // Return the rules array
      utils.writeJson(res, rules);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occured while retrieving rules.",
        500
      );
    });
};

// Get a single default rule by id
module.exports.getDefaultRuleById = function getDefaultRuleById(
  req,
  res,
  next
) {
  var ruleId = req.swagger.params["ruleId"].value;

  // Find the rule in mongo using id
  DefaultRule.findById(ruleId)
    .then(rule => {
      // If no rule found with given id, return 404
      if (!rule) {
        utils.writeJson(res, `Rule not found with id ${ruleId}.`, 404);
      }


      // Return the default rule
      utils.writeJson(res, rule);
      return;
    })
    .catch(err => {
      // Handle any unexpected error
      return;
      utils.writeJson(
        res,
        err.message || `Error retrieving rule with id ${ruleId}.`,
        500
      );
      return;
    });
};

// Get default rule for a given month
module.exports.getRuleForMonth = function getRuleForMonth(req, res, next) {
  var month = req.swagger.params["month"].value;
  var locationId = req.swagger.params["locationId"].value;

  // Get all rules
  DefaultRule.find({ "day.month": month, location_id: locationId })
    .then(rules => {
      const date = moment(); // today
      const startDate = date
        .clone()
        .startOf("month")
        .subtract(1, "day"); //set to first day of current month and subtract 1 day
      startDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
      const endDate = date
        .clone()
        .add(1, "month")
        .startOf("month")
        .subtract(1, "day"); // add 1 month set to first day and subtract 1 day
      endDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

      return CustomRule.find({
        date: { $gt: startDate.toDate(), $lt: endDate.toDate() }
      }).then(result => {
        rules.push(...result);
        return rules;
      });
    })
    .then(allRules => {
      return allRules.map(rule => rule.attachCheapesSlot(rule.toObject()));
    })
    .then(allRules => {
      utils.writeJson(res, allRules);
    })
    .catch(err => {
      console.log(err);
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occured while retrieving rules.",
        500
      );
    });
};

// Get default rule for a given month and weekday
module.exports.getRuleForDay = function getRuleForDay(req, res, next) {
  var month = req.swagger.params["month"].value;
  var weekday = req.swagger.params["weekday"].value;
  var locationId = req.swagger.params["locationId"].value;

  // Find the rule with the specified month and weekday
  DefaultRule.find({
    "day.month": month,
    "day.weekday": weekday,
    location_id: locationId
  })
    .then(rule => {
      // If no rule found with given month and weekday, return 404
      if (!rule.length) {
        return utils.writeJson(
          res,
          `Rule not found for day ${month} ${weekday}, locationId ${locationId}.`,
          404
        );
      }

      // Return the default rule
      return utils.writeJson(res, rule[0]);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message ||
        `Error retrieving rule for day ${month} ${weekday}, locationId ${locationId}.`,
        500
      );
    });
};

// Get authenticated default rule for a given month and weekday
module.exports.getRuleForDayAuth = function getRuleForDayAuth(req, res, next) {
  middleware.checkToken(req, auth => {
    console.log(auth);
    if (auth) {
      var month = req.swagger.params["month"].value;
      var weekday = req.swagger.params["weekday"].value;
      var locationId = req.swagger.params["locationId"].value;

      // Find the rule with the specified month and weekday
      DefaultRule.find({
        "day.month": month,
        "day.weekday": weekday,
        location_id: locationId
      })
        .then(rule => {
          // If no rule found with given month and weekday, return 404
          if (!rule.length) {
            return utils.writeJson(
              res,
              `Rule not found for day ${month} ${weekday}, locationId ${locationId}.`,
              404
            );
          }

          // Return the default rule
          return utils.writeJson(res, rule[0]);
        })
        .catch(err => {
          // Handle any unexpected error
          utils.writeJson(
            res,
            err.message ||
            `Error retrieving rule for day ${month} ${weekday}, locationId ${locationId}.`,
            500
          );
        });
    } else {
      utils.writeJson(res, { message: "Authorization failure" }, 401);
    }
  });
};

// Update a default rule, given it's id
module.exports.updateDefaultRule = function updateDefaultRule(req, res, next) {
  var ruleId = req.swagger.params["ruleId"].value;
  var body = req.swagger.params["body"].value;

  // Create variable to store slots
  const slots = [];

  if (!body.closed && !body.lunch_break) {
    // If day is not closed and no lunch break

    // Create moment objects of first checkin and last checkin
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );

    // Create slots between the first and last checkin
    while (first <= last) {
      // Convert checkin time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the create slot in the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add the duration into the first checkin time to move to next slot and repeat the process
      first.add(body.slot_default_duration, "minutes");
    }
  } else if (!body.closed && body.lunch_break) {
    // If day is not closed and there is a lunch break

    // Create moment objects of first and last checkin and the lunch time
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );
    const lunch = moment(
      `${body.lunch_break_from.hours}:${body.lunch_break_from.minutes}`,
      "HH:mm"
    );

    // Create slots between first checkin and lunch
    while (first < lunch) {
      // Convert checkin_time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the duration of a slot into checkin time
      first.add(body.slot_default_duration, "minutes");

      // If checkout time exceeds the lunch time, then it is not a valid slot. break the loop
      if (first > lunch) break;
      else first.subtract(body.slot_default_duration, "minutes");

      // Add the slot into the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into first checkin to move to next slot
      first.add(body.slot_default_duration, "minutes");
    }

    // Add the lunch break duration into lunch time to create slots after lunch
    lunch.add(body.lunch_break_duration, "minutes");

    // Create slots between lunch end and last checkin
    while (lunch <= last) {
      // Convert checkin_time to object
      const checkin_time = new moment(lunch).format("HH:mm").split(":");

      // Add the slot to the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into lunch end to move to next slot
      lunch.add(body.slot_default_duration, "minutes");
    }
  }

  body.slots = slots;

  // Find the rule in mongo using id
  DefaultRule.findByIdAndUpdate(ruleId, body, { new: true, useFindAndModify: false })
    .then(rule => {
      // If no rule found with given id, return 404
      if (!rule) {
        utils.writeJson(res, `Rule not found with id ${ruleId}.`, 404);
      }

      // Return the default rule
      utils.writeJson(res, rule);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error updating rule with id ${ruleId}.`,
        500
      );
    });
};

module.exports.updateDefaultRuleTimeSlot = function updateDefaultRuleTimeSlot(
  req,
  res,
  next
) {
  var ruleId = req.swagger.params["ruleId"].value;
  var timeSlotId = req.swagger.params["timeSlotId"].value;
  var body = req.swagger.params["body"].value;

  DefaultRule.findById(ruleId)
    .then(defaultRule => {
      // If no rule found with given id, return 404
      if (!defaultRule) {
        utils.writeJson(res, `Default Rule not found with id ${ruleId}.`, 404);
      }

      defaultRule.slots = defaultRule.slots.map(slot => {
        if (slot._id == timeSlotId) {
          var findFlag = 0;
          for (var property in body.data) {
            if (body.data.hasOwnProperty(property)) {
              if (slot[property] === "undefined") {
                slot.property = body.data[property];
              } else {
                slot[property] = body.data[property];
              }
            }
          }
        }
        return slot;
      });
      DefaultRule.findByIdAndUpdate(ruleId, defaultRule, {
        new: true,
        useFindAndModify: false
      })
        .then(defaultRule1 => {
          // If no rule found with given id, return 404
          if (!defaultRule1) {
            utils.writeJson(
              res,
              `Default Rule not found with id ${ruleId}.`,
              404
            );
          }

          // Return the Default rule
          utils.writeJson(res, defaultRule1);
        })
        .catch(/* istanbul ignore next */ err => {
          // Handle any unexpected error
          utils.writeJson(
            res,
            err.message || `Error updating Default rule with id ${ruleId}.`,
            500
          );
        });
    })
    .catch(/* istanbul ignore next */ err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving Default rule with id ${ruleId}.`,
        500
      );
    });
};
