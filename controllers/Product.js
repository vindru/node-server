"use strict";

var utils = require("../utils/writer.js");
var Product = require("../models/Product.model.js");

// Create a product and save to database
module.exports.addProduct = function addProduct(req, res, next) {
  var body = req.swagger.params["body"].value;

  if (!body.website) {
    body.website = false;
  }
  if (!body.checkout) {
    body.checkout = false;
  }
  if (!body.other_products) {
    body.other_products = false;
  }
  // Create the product
  const product = new Product(body);

  // Save it into database
  product
    .save()
    .then(product => {
      // Return the created product
      utils.writeJson(res, product);
    })
    .catch(err => {
      console.log(err);
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occurred while adding the product.",
        500
      );
    });
};

// Delete a product given the id
module.exports.deleteProduct = function deleteProduct(req, res, next) {
  var productId = req.swagger.params["productId"].value;

  // Find the product in mongo using id, then delete
  Product.findByIdAndDelete(productId)
    .then(product => {
      // If no product found with given id, return 404
      if (!product) {
        utils.writeJson(res, `Product not found with id ${productId}.`, 404);
      }

      // Return the product
      utils.writeJson(res, product);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error deleting product with id ${productId}.`,
        500
      );
    });
};

// Get all products from mongo
module.exports.getAllProducts = function getAllProducts(req, res, next) {
  // Get all products
  Product.find()
    .then(products => {
      // Return the products array
      utils.writeJson(res, products);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occured while retrieving products.",
        500
      );
    });
};

// Get all products by search criteria
module.exports.searchProducts = function searchProducts(req, res, next) {
  // Get all products
  var condition = {};
  var body = req.swagger.params["body"].value;
  if (body) {
    condition = body;
  }
  Product.find(condition)
    .then(products => {
      // Return the products array
      utils.writeJson(res, products);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occured while retrieving products.",
        500
      );
    });
};

// Get a single product by location id
module.exports.getProductByLocationId = function getProductById(
  req,
  res,
  next
) {
  var locationId = req.swagger.params["locationId"].value;

  // Find the product in mongo using id
  Product.find({ location_id: locationId })
    .then(product => {
      if (!product) {
        // If no product found with given id, return 404
        utils.writeJson(res, `Product not found with id ${locationId}.`, 404);
      }

      // Return the product
      utils.writeJson(res, product);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving product with id ${locationId}.`,
        500
      );
    });
};

// Get a single product by id
module.exports.getProductById = function getProductById(req, res, next) {
  var productId = req.swagger.params["productId"].value;

  // Find the product in mongo using id
  Product.findById(productId)
    .then(product => {
      if (!product) {
        // If no product found with given id, return 404
        utils.writeJson(res, `Product not found with id ${productId}.`, 404);
      }

      // Return the product
      utils.writeJson(res, product);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving product with id ${productId}.`,
        500
      );
    });
};

// Update a product, given it's id
module.exports.updateProduct = function updateProduct(req, res, next) {
  var productId = req.swagger.params["productId"].value;
  var body = req.swagger.params["body"].value;

  // Find the product in mongo using id
  Product.findByIdAndUpdate(productId, body, {
    new: true,
    useFindAndModify: false
  })
    .then(product => {
      // If no product found with given id, return 404
      if (!product) {
        utils.writeJson(res, `Product not found with id ${productId}.`, 404);
      }

      // Return the product
      utils.writeJson(res, product);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error updating product with id ${productId}.`,
        500
      );
    });
};
