"use strict";

var utils = require("../utils/writer.js");
var CustomRule = require("../models/CustomRule.model.js");

const moment = require("moment");
moment().format();

// Create a custom rule and save to database
module.exports.addCustomRule = function addCustomRule(req, res, next) {
  try {
  var body = req.swagger.params["body"].value;

  // Create variable to store slots
  const slots = [];

  if (!body.closed && !body.lunch_break) {
    // If day is not closed and no lunch break

    // Create moment objects of first checkin and last checkin
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );

    // Create slots between the first and last checkin
    while (first <= last) {
      // Convert checkin time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the create slot in the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add the duration into the first checkin time to move to next slot and repeat the process
      first.add(body.slot_default_duration, "minutes");
    }
  } else if (!body.closed && body.lunch_break) {
    // If day is not closed and there is a lunch break

    // Create moment objects of first and last checkin and the lunch time
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );
    const lunch = moment(
      `${body.lunch_break_from.hours}:${body.lunch_break_from.minutes}`,
      "HH:mm"
    );

    // Create slots between first checkin and lunch
    while (first < lunch) {
      // Convert checkin_time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the duration of a slot into checkin time
      first.add(body.slot_default_duration, "minutes");

      // If checkout time exceeds the lunch time, then it is not a valid slot. break the loop
      if (first > lunch) break;
      else first.subtract(body.slot_default_duration, "minutes");

      // Add the slot into the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into first checkin to move to next slot
      first.add(body.slot_default_duration, "minutes");
    }

    // Add the lunch break duration into lunch time to create slots after lunch
    lunch.add(body.lunch_break_duration, "minutes");

    // Create slots between lunch end and last checkin
    while (lunch <= last) {
      // Convert checkin_time to object
      const checkin_time = new moment(lunch).format("HH:mm").split(":");

      // Add the slot to the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into lunch end to move to next slot
      lunch.add(body.slot_default_duration, "minutes");
    }
  }

  // Store created slots in the rule object
  body.slots = slots;

  // Create the rule
  const customRule = new CustomRule(body);
//  console.log(body);
  // Save it into database

  const firstHourOfDay = moment(body.date).startOf("day");
  const lastHourOfDay = moment(body.date).endOf("day");
  // Find the rule with the specified date
  /*CustomRule.find({
    date: { $gt: firstHourOfDay.toDate(), $lt: lastHourOfDay.toDate() },
    location_id: locationId
  })*/


  CustomRule
  .findOneAndUpdate({
    date: { $gte: firstHourOfDay.toDate(), $lt: lastHourOfDay.toDate() },
    location_id: body.location_id
  },{$set:body}, { upsert: true ,useFindAndModify: false,new: true})
    .then(customRule => {
      // Return the created rule
      utils.writeJson(res, customRule);
    })
    .catch(err => {
      console.log(err)
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occurred while adding the custom rule.",
        500
      );
    });
  }
  catch(err) {
    console.log(err)
      // Handle any unexpected error
      utils.writeJson(
        res,
        {message: err.message || "Some error occurred while adding the custom rule."},
        500
      );
  }
};

// Delete a custom rule given the id
module.exports.deleteCustomRule = function deleteCustomRule(req, res, next) {
  var ruleId = req.swagger.params["ruleId"].value;

  // Find the rule in mongo using id, then delete
  CustomRule.findByIdAndDelete(ruleId)
    .then(customRule => {
      // If no rule found with given id, return 404
      if (!customRule) {
        utils.writeJson(res, `Custom Rule not found with id ${ruleId}.`, 404);
      }

      // Return the custom rule
      utils.writeJson(res, customRule);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error deleting custom rule with id ${ruleId}.`,
        500
      );
    });
};

// Get all custom rules from mongo
module.exports.getAllCustomRules = function getAllCustomRules(req, res, next) {
  // Get all rules
  CustomRule.find()
    .then(customRules => {
      // Return the rules array
      utils.writeJson(res, customRules);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || "Some error occured while retrieving custom rules.",
        500
      );
    });
};

// Get a single custom rule by id
module.exports.getCustomRuleById = function getCustomRuleById(req, res, next) {
  var ruleId = req.swagger.params["ruleId"].value;

  // Find the rule in mongo using id
  CustomRule.findById(ruleId)
    .then(customRule => {
      // If no rule found with given id, return 404
      if (!customRule) {
        utils.writeJson(res, `Custom Rule not found with id ${ruleId}.`, 404);
      }

      // Return the custom rule
      utils.writeJson(res, customRule);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving custom rule with id ${ruleId}.`,
        500
      );
    });
};

// Get custom rule for a given date
module.exports.getRuleForDate = function getRuleForDate(req, res, next) {
  var date = req.swagger.params["date"].value;
  var locationId = req.swagger.params["locationId"].value;

  //const firstHourOfDay = moment(date.toISOString().split('T')[0]).subtract('day', 1).startOf('day');
  const firstHourOfDay = moment(date).startOf("day");
  const lastHourOfDay = moment(date).endOf("day");
  // Find the rule with the specified date
  /*CustomRule.find({
    date: { $gt: firstHourOfDay.toDate(), $lt: lastHourOfDay.toDate() },
    location_id: locationId
  })*/
  CustomRule.find({
    date: { $gte: firstHourOfDay.toDate(), $lt: lastHourOfDay.toDate() },
    location_id: locationId
  })
    .then(customRule => {
      // If no rule found with given date, return 404
      if (!customRule.length) {
        return utils.writeJson(
          res,
          `Custom Rule not found for date ${date}.`,
          404
        );
      }

      // Return the custom rule
      utils.writeJson(res, customRule[0]);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving custom rule for date ${date}.`,
        500
      );
    });
};

// Update a custom rule, given it's id
module.exports.updateCustomRule = function updateCustomRule(req, res, next) {
  var ruleId = req.swagger.params["ruleId"].value;
  var body = req.swagger.params["body"].value;

  // Create variable to store slots
  const slots = [];

  if (!body.closed && !body.lunch_break) {
    // If day is not closed and no lunch break

    // Create moment objects of first checkin and last checkin
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );

    // Create slots between the first and last checkin
    while (first <= last) {
      // Convert checkin time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the create slot in the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add the duration into the first checkin time to move to next slot and repeat the process
      first.add(body.slot_default_duration, "minutes");
    }
  } else if (!body.closed && body.lunch_break) {
    // If day is not closed and there is a lunch break

    // Create moment objects of first and last checkin and the lunch time
    const first = moment(
      `${body.first_checkin.hours}:${body.first_checkin.minutes}`,
      "HH:mm"
    );
    const last = moment(
      `${body.last_checkin.hours}:${body.last_checkin.minutes}`,
      "HH:mm"
    );
    const lunch = moment(
      `${body.lunch_break_from.hours}:${body.lunch_break_from.minutes}`,
      "HH:mm"
    );

    // Create slots between first checkin and lunch
    while (first < lunch) {
      // Convert checkin_time to object
      const checkin_time = new moment(first).format("HH:mm").split(":");

      // Add the duration of a slot into checkin time
      first.add(body.slot_default_duration, "minutes");

      // If checkout time exceeds the lunch time, then it is not a valid slot. break the loop
      if (first > lunch) break;
      else first.subtract(body.slot_default_duration, "minutes");

      // Add the slot into the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into first checkin to move to next slot
      first.add(body.slot_default_duration, "minutes");
    }

    // Add the lunch break duration into lunch time to create slots after lunch
    lunch.add(body.lunch_break_duration, "minutes");

    // Create slots between lunch end and last checkin
    while (lunch <= last) {
      // Convert checkin_time to object
      const checkin_time = new moment(lunch).format("HH:mm").split(":");

      // Add the slot to the slots variable
      slots.push({
        checkin_time: {
          hours: parseInt(checkin_time[0]),
          minutes: parseInt(checkin_time[1])
        },
        duration: body.slot_default_duration,
        number_of_spaces: body.slot_default_spaces,
        deposit_price: body.slot_default_deposit_price,
        invoice_price: body.slot_default_invoice_price
      });

      // Add duration into lunch end to move to next slot
      lunch.add(body.slot_default_duration, "minutes");
    }
  }

  body.slots = slots;

  // Find the rule in mongo using id
  CustomRule.findByIdAndUpdate(ruleId, body, {
    new: true,
    useFindAndModify: false
  })
    .then(customRule => {
      // If no rule found with given id, return 404
      if (!customRule) {
        utils.writeJson(res, `Custom Rule not found with id ${ruleId}.`, 404);
      }

      // Return the custom rule
      utils.writeJson(res, customRule);
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error updating custom rule with id ${ruleId}.`,
        500
      );
    });
};
module.exports.updateCustomRuleTimeSlot = function updateCustomRuleTimeSlot(
  req,
  res,
  next
) {
  var ruleId = req.swagger.params["ruleId"].value;
  var timeSlotId = req.swagger.params["timeSlotId"].value;
  var body = req.swagger.params["body"].value;
  //console.log(body.data)
  CustomRule.findById(ruleId)
    .then(customRule => {
      // If no rule found with given id, return 404
      if (!customRule) {
        utils.writeJson(res, `Custom Rule not found with id ${ruleId}.`, 404);
      }

      customRule.slots = customRule.slots.map(slot => {
        if (slot._id == timeSlotId) {

          var findFlag = 0;
          for (var property in body.data) {
            if (body.data.hasOwnProperty(property)) {

              if (slot[property] === "undefined") {
                slot.property = body.data[property];
              } else {
                slot[property] = body.data[property];
              }
            }
          }
        }
        return slot;
      });

      CustomRule.findOneAndUpdate({ _id: ruleId }, { $set: customRule }, { new: true })
        .then(customRule1 => {
          // If no rule found with given id, return 404
          if (!customRule1) {
            utils.writeJson(
              res,
              `Custom Rule not found with id ${ruleId}.`,
              404
            );
          }

          // Return the custom rule
          utils.writeJson(res, customRule1);
        })
        .catch(err => {
          // Handle any unexpected error
          utils.writeJson(
            res,
            err.message || `Error updating custom rule with id ${ruleId}.`,
            500
          );
        });
    })
    .catch(err => {
      // Handle any unexpected error
      utils.writeJson(
        res,
        err.message || `Error retrieving custom rule with id ${ruleId}.`,
        500
      );
    });
};
