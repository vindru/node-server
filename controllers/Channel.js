'use strict';

var utils = require('../utils/writer.js');
var channelSchema = require('../models/Channel.model.js');
var sourceSchema = require('../models/Source.model.js');
// Create a channel and save to database
module.exports.addChannel = function addChannel (req, res, next) {
  var body = req.swagger.params['body'].value;

  // Create the channel
  const channel = new channelSchema(body);

  // Save it into database
  channel.save()
  .then(channel => {

    // Return the created channel
    utils.writeJson(res, channel);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || 'Some error occurred while adding the channel.', 500);
  });
};

// Delete a channel given the id
module.exports.deleteChannel = function deleteChannel (req, res, next) {
  var channelId = req.swagger.params['channelId'].value;

  // Find the channel in mongo using id, then delete
  channelSchema.findByIdAndDelete(channelId)
  .then(channel => {

    // If no channel found with given id, return 404
    if(!channel) {
      utils.writeJson(res, `Channel not found with id ${channelId}.`, 404);
    }

    // Return the channel
    utils.writeJson(res, channel);
  }).catch(err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || `Error deleting channel with id ${channelId}.`, 500);
  });
};

// Get all channels from mongo
module.exports.getAllChannels = function getAllChannels (req, res, next) {

  // Get all channels
  channelSchema.find()
  .then(channels => {

    // Return the channels array
    utils.writeJson(res, channels);
  }).catch(err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || 'Some error occured while retrieving channels.', 500);
  });
};

// Get a single channel by location id
module.exports.getChannelByLocationId = function getChannelById (req, res, next) {
  var location_id = req.swagger.params['location_id'].value;

  // Find the channel in mongo using id
  channelSchema.find({ "location_id": location_id })
  .then(channel => {
    if(!channel) {

      // If no channel found with given id, return 404
      utils.writeJson(res, `Channel not found with id ${location_id}.`, 404);
    }

    // Return the channel
    utils.writeJson(res, channel);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || `Error retrieving channel with id ${location_id}.`, 500);
  });
};

// Get a single channel by id
module.exports.getChannelById = function getChannelById (req, res, next) {
  var channelId = req.swagger.params['channelId'].value;

  // Find the channel in mongo using id
  channelSchema.findById(channelId)
  .then(channel => {
    if(!channel) {

      // If no channel found with given id, return 404
      utils.writeJson(res, `Channel not found with id ${channelId}.`, 404);
    }

    // Return the channel
    utils.writeJson(res, channel);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || `Error retrieving channel with id ${channelId}.`, 500);
  });
};

// Update a channel, given it's id
module.exports.updateChannel = function updateChannel (req, res, next) {
  var channelId = req.swagger.params['channelId'].value;
  var body = req.swagger.params['body'].value;

  // Find the channel in mongo using id
  channelSchema.findByIdAndUpdate(channelId, body, {new: true, useFindAndModify: false})
  .then(channel => {

    // If no channel found with given id, return 404
    if(!channel) {
      utils.writeJson(res, `Channel not found with id ${channelId}.`, 404);
    }

    // Return the channel
    utils.writeJson(res, channel);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || `Error updating channel with id ${channelId}.`, 500);
  });
};


// Get all sources
module.exports.getAllSources = function getAllSources (req, res, next) {

  // Get all channels
  sourceSchema.find()
  .then(source => {

    // Return the channels array
    utils.writeJson(res, source);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || 'Some error occured while retrieving sources.', 500);
  });
};

// Create a source and save to database
module.exports.addSource = function addSource (req, res, next) {
  var body = req.swagger.params['body'].value;

  // Create the source
  const source = new sourceSchema(body);

  // Save it into database
  source.save()
  .then(source => {

    // Return the created source
    utils.writeJson(res, source);
  }).catch(/* istanbul ignore next */ err => {

    // Handle any unexpected error
    utils.writeJson(res, err.message || 'Some error occurred while adding the source.', 500);
  });
};