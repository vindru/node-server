const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const app = require('../index.js');

describe('\n\nRunning Default Rule tests\n\n', function() {
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
  it("Create Availability for "+formatDate(new Date()), (done) => {
    var date = new Date();
    global.date = date;
    weekdays = ['sun','mon','tue','wed','thu','fri','sat']
        chai.request(app)
            .post(`/api/v1/defaultRule`)
            .send({
              "slot_default_duration": 60,
              "slot_default_spaces": 150,
              "slot_default_deposit_price": 50,
              "slot_default_invoice_price": 200,
              "closed": false,
              "first_checkin": {
                "hours": 1,
                "minutes": 0
              },
              "last_checkin": {
                "hours": 23,
                "minutes": 0
              },
              "lunch_break": false,
              "day": {
                "month": date.getMonth() +1,
                "weekday": weekdays[date.getDay()]
              },
              "location_id": 1
            })
            .end((err, res) => {
              if(err)
                console.log("ERROR",err)
             // console.log("RESPONSE",res.text);
              expect(res).to.has.status(200);
              const body = JSON.parse(res.text);
              expect(body).to.be.an('object');
              global.id = body._id;
              done()

            });
   
  });

 

it("get one default rule", (done) => {
    chai.request(app)
    .get(`/api/v1/defaultRule/${global.id}`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
      global.time_slot_id = body.slots[0]._id;
      done();
    });
  });

  it("update one default rule", (done) => {
    chai.request(app)
    .put(`/api/v1/defaultRule/${global.id}`)
    .send({
      "lunch_break_from": {
        "hours": 13,
        "minutes": 8
      },
      "lunch_break_duration": 45,
      "slot_default_duration": 60,
      "slot_default_spaces": 10,
      "slot_default_deposit_price": 50,
      "slot_default_invoice_price": 200,
      "closed": false,
      "first_checkin": {
        "hours": 13,
        "minutes": 8
      },
      "last_checkin": {
        "hours": 13,
        "minutes": 8
      },
      "lunch_break": true,
      "day": {
        "month": 1,
        "weekday": "mon"
      },
      "location_id": 1
    })
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
     // console.log(body)
   
      expect(body).have.property('lunch_break_duration').equals(45);
      done();
    });
  });

  it("should update the number of spaces of a purticular time slot", done => {
    var time_slot = {
      hours: 10,
      minutes: 0
    };
    chai
      .request(app)
      .put(`/api/v1/defaultRule/${global.id}/timeSlot/${global.time_slot_id}`)
      .send({
        data: {
          number_of_spaces: 30
        }
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        body.slots.forEach(slot => {
          if (slot._id === global.time_slot_id) {
            expect(slot)
              .have.property("number_of_spaces")
              .equal(30);
          }
        });

        done();
      });
  });
  
    it("get all default rules", (done) => {
    chai.request(app)
     .get(`/api/v1/defaultRule`)
     .end((err, res) => {
       expect(res).to.has.status(200);
       expect(JSON.parse(res.text)).to.be.an('array');
       done();
     });
   });

  it("delete one default rule", (done) => {
    chai.request(app)
    .delete(`/api/v1/defaultRule/${global.id}`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
      done();
    });
  }); 
  it("Run default rule for all weekdays of entire year", (done) => {
    weekdays = ['mon','tue','wed','thu','fri','sat','sun']
    for(var i = 1; i < 13; i++) {
      for (var j = 0; j < weekdays.length; j++) {
        chai.request(app)
            .post(`/api/v1/defaultRule`)
            .send({
            
              "slot_default_duration": 60,
              "slot_default_spaces": 150,
              "slot_default_deposit_price": 50,
              "slot_default_invoice_price": 200,
              "closed": false,
              "first_checkin": {
                "hours": 1,
                "minutes": 0
              },
              "last_checkin": {
                "hours": 23,
                "minutes": 0
              },
              "lunch_break": false,
              "day": {
                "month": i,
                "weekday": weekdays[j]
              },
              "location_id": 1
            })
            .end((err, res) => {
              expect(res).to.has.status(200);
              const body = JSON.parse(res.text);
              expect(body).to.be.an('object');
              global.id = body._id;
             // console.log(body.day)
              if (body.day.month === 12 && body.day.weekday == 'sun') {
                done();
              }

            });
      };


    }
  });
});
