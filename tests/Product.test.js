const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const expect = chai.expect;
const app = require("../index.js");

describe("\n\nRunning Product tests\n\n", function() {
  it("get all products", done => {
    chai
      .request(app)
      .get(`/api/v1/product`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        expect(JSON.parse(res.text)).to.be.an("array");
        done();
      });
  });

  it("add one product", done => {
    chai
      .request(app)
      .post(`/api/v1/product`)
      .send({
        internal_code: "in_photo_video",
        title: "Inside Photo & Video",
        confirmation_title: "Inside Photo & Video",
        extra_aircraft_slot_required: 1,
        description: "This is a long description.",
        price: 50,
        image: "data:image/png;base64,iVBORw0KGgoAAAANSUh"
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        global.id = body._id;
        done();
      });
  });

  it("get one product", done => {
    chai
      .request(app)
      .get(`/api/v1/product/${global.id}`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        done();
      });
  });

  it("update one product", done => {
    chai
      .request(app)
      .put(`/api/v1/product/${global.id}`)
      .send({
        internal_code: "in_photo",
        title: "Inside Photo & Video",
        confirmation_title: "Inside Photo & Video",
        extra_aircraft_slot_required: 1,
        description: "This is a long description.",
        price: 50,
        image: "data:image/png;base64,iVBORw0KGgoAAAANSUh"
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        expect(body)
          .have.property("internal_code")
          .equals("in_photo");
        done();
      });
  });

  it("delete one product", done => {
    chai
      .request(app)
      .delete(`/api/v1/product/${global.id}`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        done();
      });
  });
});
