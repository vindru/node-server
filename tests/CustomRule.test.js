const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const expect = chai.expect;
const app = require("../index.js");


describe("\n\nRunning Custom Rule tests\n\n", function() {
  const locationId = 1;
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
  
  it("add one custom rule for "+formatDate(new Date()), done => {
    var date = new Date();
    global.date = date;
    chai
      .request(app)
      .post(`/api/v1/customRule`)
      .send({
        lunch_break_from: {
          hours: 13,
          minutes: 0
        },
        lunch_break_duration: 60,
        slot_default_duration: 60,
        slot_default_spaces: 10,
        slot_default_deposit_price: 50,
        slot_default_invoice_price: 200,
        closed: false,
        first_checkin: {
          hours: 0,
          minutes: 0
        },
        last_checkin: {
          hours: 23,
          minutes: 0
        },
        lunch_break: true,
        date: date,
        location_id: locationId
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        global.id = body._id;
        done();
      });
  });

 it("get one custom rule", done => {
    chai
      .request(app)
      .get(`/api/v1/customRule/${global.id}`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        done();
      });
  });

  it("update one custom rule", done => {
    chai
      .request(app)
      .put(`/api/v1/customRule/${global.id}`)
      .send({
        lunch_break_from: {
          hours: 13,
          minutes: 0
        },
        lunch_break_duration: 120,
        slot_default_duration: 60,
        slot_default_spaces: 10,
        slot_default_deposit_price: 50,
        slot_default_invoice_price: 200,
        closed: false,
        first_checkin: {
          hours: 1,
          minutes: 0
        },
        last_checkin: {
          hours: 22,
          minutes: 0
        },
        lunch_break: true,
        date: date,
        location_id: locationId
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        expect(body)
          .have.property("lunch_break_duration")
          .equals(120);
        done();
      });
  });
 
  it("Select a booking by date and location", done => {
    chai
      .request(app)
      .get(`/api/v1/customRule/date/${global.date.toISOString()}/locationId/${locationId}`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body).have.property("slots");
        expect(body._id).to.be.equal(global.id);
        global.time_slot_id = body.slots[0]._id;
        done();
      });
  });
 
  it("should update the number of spaces of a particular time slot & lock status", done => {
    var time_slot = {
      hours: 10,
      minutes: 0
    };
    chai
      .request(app)
      .put(`/api/v1/customRule/${global.id}/timeSlot/${global.time_slot_id}`)
      .send({
        data: {
          number_of_spaces: 30,
          lock_status: "red"
        }
      })
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        body.slots.forEach(slot => {
          if (slot._id === global.time_slot_id) {
            expect(slot)
              .have.property("number_of_spaces")
              .equal(30);
              expect(slot)
              .have.property("lock_status")
              .equal("red");
          }
        });

        done();
      });
  });


  it("get all custom rules", done => {
    chai
      .request(app)
      .get(`/api/v1/customRule`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        expect(JSON.parse(res.text)).to.be.an("array");
        done();
      });

  });

  it("delete one custom rule", done => {
    chai
      .request(app)
      .delete(`/api/v1/customRule/${global.id}`)
      .end((err, res) => {
        expect(res).to.has.status(200);
        const body = JSON.parse(res.text);
        expect(body).to.be.an("object");
        expect(body)
          .have.property("_id")
          .equals(global.id);
        done();
      });
  }); 
});

