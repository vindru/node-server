const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const app = require('../index.js');

describe('\n\nRunning channel tests\n\n', function() {

  it("add one channel", (done) => {
    chai.request(app)
    .post(`/api/v1/channel`)
    .send({
      "channel_name": "Daredevil Tours",
      "accept_credit_card": false,
      "accept_cash": true,
      "must_pay_before_jump": false,
      "thirty_days_after_booking": true,
      "thirty_days_after_fulfillment": false,
      "fixed_price": 199,
      "percentage_discount": 25,
      "products_allowed": [
        "5d0c76146f2b6525d4a8647f",
        "5d12cb06d3b1320e1dbb34fd"
      ]
    })
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      global.id = body._id;
      done();
    });
  });

   it("get one channel", (done) => {
    chai.request(app)
    .get(`/api/v1/channel/${global.id}`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
      done();
    });
  });

  it("update one channel", (done) => {
    chai.request(app)
    .put(`/api/v1/channel/${global.id}`)
    .send({
      "channel_name": "Call Center",
      "accept_credit_card": false,
      "accept_cash": true,
      "must_pay_before_jump": false,
      "thirty_days_after_booking": true,
      "thirty_days_after_fulfillment": false,
      "fixed_price": 199,
      "percentage_discount": 52,
      "products_allowed": [
        "5d0c76146f2b6525d4a8647f",
        "5d12cb06d3b1320e1dbb34fd"
      ]
    })
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
      expect(body).have.property('percentage_discount').equals(52);
      done();
    });
  });

  it("get all channels", (done) => {
    chai.request(app)
    .get(`/api/v1/channel`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      expect(JSON.parse(res.text)).to.be.an('array');
      done();
    });
  });


  it("delete one channel", (done) => {
    chai.request(app)
    .delete(`/api/v1/channel/${global.id}`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      expect(body).have.property('_id').equals(global.id);
      done();
    });
  }); 

  it("add one source", (done) => {
    chai.request(app)
    .post(`/api/v1/source`)
    .send({
      "source_name": "WWW"
    })
    .end((err, res) => {
      expect(res).to.has.status(200);
      const body = JSON.parse(res.text);
      expect(body).to.be.an('object');
      global.id = body._id;
      done();
    });
  });
  it("get all sources", (done) => {
    chai.request(app)
    .get(`/api/v1/source`)
    .end((err, res) => {
      expect(res).to.has.status(200);
      var sources = JSON.parse(res.text);
      expect(sources).to.be.an('array');
      done();
    });
  });
});