const mongoose = require("mongoose");
const nanoid = require("nanoid");

const f = () => nanoid(3);

const ProductSchema = mongoose.Schema(
  {
    internal_code: { type: String, unique: true, default: f },
    location_id: Number,
    title: String,
    confirmation_title: String,
    description: String,
    image: String,
    price: Number,
    website: { type: Boolean, index: true, default: false },
    checkout: { type: Boolean, index: true, default: false },
    other_products: { type: Boolean, index: true, default: false },
    extra_aircraft_slot_required: Number
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Product", ProductSchema);
