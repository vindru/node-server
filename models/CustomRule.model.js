const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false);

const SlotShema = mongoose.Schema({
  checkin_time: {
    hours: Number,
    minutes: Number
  },
  duration: Number,
  number_of_spaces: Number,
  deposit_price: Number,
  invoice_price: Number,
  lock_status: { type: String, default: "green" }
});

const CustomRuleSchema = mongoose.Schema(
  {
    location_id: Number,
    date: Date,
    closed: Boolean,
    first_checkin: {
      hours: Number,
      minutes: Number
    },
    last_checkin: {
      hours: Number,
      minutes: Number
    },
    slot_default_duration: Number,
    slot_default_spaces: Number,
    slot_default_deposit_price: Number,
    slot_default_invoice_price: Number,
    slots: [SlotShema],
    lunch_break: Boolean,
    lunch_break_from: {
      hours: Number,
      minutes: Number
    },
    lunch_break_duration: Number
  },
  {
    timestamps: true
  }
);

CustomRuleSchema.methods.attachCheapesSlot = function (rule) {
  let lowest = this.slots[0];
  let biggest = this.slots[0];
  this.slots.forEach(current => {
    if (current.invoice_price < lowest.invoice_price) {
      lowest = current;
    }
    if (current.invoice_price > biggest.invoice_price) {
      biggest = current;
    }
  });
  rule.cheapest_slot = lowest;

  return rule;
};

module.exports = mongoose.model("CustomRule", CustomRuleSchema);
