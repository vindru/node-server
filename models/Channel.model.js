const mongoose = require('mongoose');

const ChannelSchema = mongoose.Schema({
  location_id: Number,
  channel_name: String,
  accept_credit_card: Boolean,
  accept_cash: Boolean,
  must_pay_before_jump: Boolean,
  thirty_days_after_booking: Boolean,
  thirty_days_after_fulfillment: Boolean,
  fixed_price: Number,
  percentage_discount: Number,
  products_allowed: Array,
}, {
  timestamps: true
});

module.exports = mongoose.model('Channel', ChannelSchema);