require('dotenv').config()
const {FusionAuthClient} = require('fusionauth-node-client');
const client = new FusionAuthClient(
    process.env.MASTER_KEY, //secret master key
    process.env.FUSIONAUTH_SERVER
);
//This application ID was created using the Admin UI for FusionAuth
const applicationId = '101576d4-7929-4375-a6e3-9db074964486';

module.exports = {
    client: client,
    applicationId:applicationId
  }