
let config = require('../config/config');

exports.checkToken = (req,callback) => {
  const authorizationHeader = req.headers.authorization;
  if (authorizationHeader) {
    const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
    if (token) {
      console.log("This is token " + token.substring(0, 10))
      config.client.validateJWT(token)
          .then(function (clientResponse) {
            console.log("SUCCESS " +clientResponse)
              callback(true);
          })
          .catch(function (error) {
            console.log(error)
           callback(false)
          });
    } else {
        callback(false)
    }
  } else {
      callback(false)
  }

};

